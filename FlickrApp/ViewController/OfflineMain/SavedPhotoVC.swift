//
//  SavedPhotoVC.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/18/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import UIKit

class SavedPhotoVC: UIViewController {

    //MARK: - Outlets & Variables
    /***************************************************************/
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var itemArray = OfflineFunctions.instance.getDataFromDB()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        setupWithPhotos()
        print("FILE SIRECTORY \(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.reloadData()
    }
    
    //MARK: - Functions
    /***************************************************************/
    func setupWithPhotos() -> Void {
        
        spinner.isHidden = false
        spinner.startAnimating()
        
        if itemArray.count > 0 {
            self.collectionView.reloadData()
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        } else {
            
            let alert = UIAlertController(title: "OFFLINE", message: "You do not have any photo saved offline", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                self.spinner.isHidden = true
                self.spinner.stopAnimating()
            })
            
            self.present(alert, animated: true)
        }
    }
    

    //MARK: - IBActions
    /***************************************************************/
    
    @IBAction func closePressed(_ sender: Any) {
        performSegue(withIdentifier: UNWIND, sender: nil)
    }
    
}



//MARK: - Extention
/***************************************************************/

extension SavedPhotoVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SAVED_CELL, for: indexPath) as? SavedCell {
            
            let flickerPhoto = itemArray[indexPath.row]
            
            cell.configueCell(flickerPhotoModel: flickerPhoto, index: indexPath.item)
            return cell
        }
        return SavedCell()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // setting cells based on screen size
        
        var numOfColums : CGFloat = 2
        if UIScreen.main.bounds.width > 320 {
            numOfColums = 1
        }
        
        let spaceBtwCells : CGFloat = 10
        let padding: CGFloat = 40
        let cellDimension = ((collectionView.bounds.width - padding) - (numOfColums - 1) * spaceBtwCells) / numOfColums
        
        return CGSize(width: cellDimension, height: cellDimension)
        
    }
    
    
    //selecting from the collectionview
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        let offlineList = OfflineFunctions.instance.offlineItems?[indexPath.item]
        OfflineFunctions.instance.selectedImage = offlineList
        performSegue(withIdentifier: OFFLINE_TO_SAVED_DETAIL_VC_SEGUE, sender:indexPath)
        NotificationCenter.default.post(name: NOTIFY_PHOTO_SELECTED, object: nil)

    }
    
}


