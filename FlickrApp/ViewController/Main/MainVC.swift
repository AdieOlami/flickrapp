//
//  ViewController.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/17/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import UIKit
import SDWebImage

class MainVC: UIViewController {
    
    //MARK: - Outlets & Variables
    /***************************************************************/
    @IBOutlet weak var collectionView: UICollectionView!
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {}
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var itemArray = OfflineFunctions.instance.getDataFromDB()
    var flickrArray = ServiceProvider.instance.flickerPhotos
    
    var offline: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        setupWithPhotos()
        
        // use to get directory on your own computer
//        print("FILE SIRECTORY \(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.reloadData()
    }
    
    //MARK: - Functions
    /***************************************************************/
    func setupWithPhotos() -> Void {
        
        spinner.isHidden = false
        spinner.startAnimating()
        
        ServiceProvider.instance.getPhotos {[weak self] (success, error) in
            if success {
                self?.collectionView.reloadData()
                self?.spinner.isHidden = true
                self?.spinner.stopAnimating()
                
                self?.offline = false
            }
            
            if error != nil {
                
                let alert = UIAlertController(title: "ERROR", message: "\(String(describing: error!))", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    self?.spinner.isHidden = true
                    self?.spinner.stopAnimating()
                    self?.collectionView.reloadData()
                    self?.offline = true
                })
                
                self?.present(alert, animated: true)
            }
        }
    }
    
    
    //MARK: - IBActions
    /***************************************************************/
    
    @IBAction func reloadPressed(_ sender: Any) {
        setupWithPhotos()
    }
    


}



//MARK: - CollectionView Extention
/***************************************************************/

extension MainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PHOTO_CELL, for: indexPath) as? MainCell {
            
            cell.offline = offline
            if !offline {
                
                 let flickerPhoto = ServiceProvider.instance.flickerPhotos[indexPath.row]
                cell.configueCell(flickerPhotoModel: flickerPhoto, index: indexPath.item)
                return cell
            } else {
                
                let flickerPhoto = itemArray[indexPath.row]
                cell.configueOfflineCell(flickerPhotoModel: flickerPhoto, index: indexPath.item)
                return cell
            }
        }
        return MainCell()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if !offline {
            return ServiceProvider.instance.flickerPhotos.count
        } else {
            return itemArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // setting cells based on screen size
        
        // Handling view on iPad
        var numOfColums : CGFloat = 1
        if UIScreen.main.bounds.width > 450 {
            numOfColums = 2
        }
        
        let spaceBtwCells : CGFloat = 10
        let padding: CGFloat = 40
        let cellDimension = ((collectionView.bounds.width - padding) - (numOfColums - 1) * spaceBtwCells) / numOfColums
        
        return CGSize(width: cellDimension, height: cellDimension)
        
    }
    
   
    //selecting from the collectionview
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {


        if !offline {
            let flickerPhoto = ServiceProvider.instance.flickerPhotos[indexPath.item]
            ServiceProvider.instance.selectedPhoto = flickerPhoto
            performSegue(withIdentifier: DETAIL_VC_SEGUE, sender:indexPath)
        } else {
            let offlineList = OfflineFunctions.instance.offlineItems?[indexPath.item]
            OfflineFunctions.instance.selectedImage = offlineList
            performSegue(withIdentifier: MAIN_TO_SAVED_DETAIL_VC_SEGUE, sender:indexPath)
        }
        NotificationCenter.default.post(name: NOTIFY_PHOTO_SELECTED, object: nil)
        
        
    }
    
    //MARK: - Segue for calling closure
    /***************************************************************/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == SAVED_DETAIL_VC) {
            
            let popUp = segue.destination as! SavedDetailVC
            popUp.doneSaving = { [weak self] in
                self?.collectionView.reloadData()
            }
            
        }
    }
    
}









