//
//  DetailVC.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/17/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    //MARK: - Outlets & Variables
    /***************************************************************/
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDidLoadFunc()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewDidAppearFunc()
       
    }
    
    
    //MARK: - Functions
    /***************************************************************/
    
    //1
    func viewDidLoadFunc() -> Void {
        guard let photoTitle = ServiceProvider.instance.selectedPhoto?.title else {return}
        guard let imageString = ServiceProvider.instance.selectedPhoto?.flickerImg else {return}
        
        titleLabel.text = photoTitle
        imgView.sd_setImage(with: URL(string: imageString) )
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailVC.photoSelected(_:)), name: NOTIFY_PHOTO_SELECTED, object: nil)
    }
    
    //2
    func viewDidAppearFunc() -> Void {
        guard let photo = imgView.image else {return}
        guard var itemPhoto = photo.pngData() else {return}
        let imageSize: Int = itemPhoto.count
        
        if imageSize > 16252928 {
            itemPhoto = photo.jpegData(compressionQuality: 0.5)!
        }
        
        OfflineFunctions.instance.getDataFromDB().forEach { (modelList) in
            if modelList.photo != itemPhoto {
                saveBtn.isEnabled = true
            } else {
                saveBtn.isEnabled = false
                saveBtn.setTitle("Saved Already", for: .normal)
            }
        }
    }
    
    //3
    @objc func photoSelected(_ notify: Notification) -> Void {
        
        getDetail()
    }
    
    //4
    func getDetail() -> Void {
        
        guard let photoTitle = ServiceProvider.instance.selectedPhoto?.title else {return}
        titleLabel.text = photoTitle
    }
    
    //5
    func saveBtnFunc() -> Void {
        guard let photo = imgView.image else {return}
        guard var itemPhoto = photo.pngData() else {return}
        guard let title = titleLabel.text else {return}
        let imageSize: Int = itemPhoto.count
        
        if imageSize > 16252928 {
            itemPhoto = photo.jpegData(compressionQuality: 0.5)!
        }
        
        OfflineFunctions.instance.getDataFromDB().forEach { (modelList) in
            if modelList.photo != itemPhoto {
                
                saveBtn.isEnabled = true
            } else {
                saveBtn.isEnabled = false
                saveBtn.setTitle("Saved Already", for: .normal)
            }
        }
        
        OfflineFunctions.instance.createOfflineList(createdDate: Date(), photo: itemPhoto, title: title, completion: { (success, error) in
            if success {
                Toast.show(message: "Saved Successfully", controller: self)
            }
        })
    }
    
    //MARK: - IBActions
    /***************************************************************/
    @IBAction func saveBtnPressed(_ sender: Any) {
        saveBtnFunc()
    }
    
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }


}
