//
//  SavedDetailVC.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/18/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import UIKit

class SavedDetailVC: UIViewController {
    
    //MARK: - Outlets & Variables
    /***************************************************************/
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    var todoItemImage = OfflineFunctions.instance.selectedImage
    var doneSaving: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        photoData()
    }
    
    //MARK: - Functions
    /***************************************************************/
    fileprivate func photoData() {
        if let photoData = todoItemImage?.photo {
            imgView?.image = UIImage(data: photoData)?.fixOrientation()
            titleLabel.text = todoItemImage?.title
            
        }
    }
    
    //MARK: - IBActions
    /***************************************************************/
    @IBAction func saveBtnPressed(_ sender: Any) {
        OfflineFunctions.instance.deleteFromDb(object: todoItemImage!)
        if let doneSaving = doneSaving {
            doneSaving()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
