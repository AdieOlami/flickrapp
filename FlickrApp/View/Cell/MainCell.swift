//
//  MainCell.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/17/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import UIKit
import SDWebImage

class MainCell: UICollectionViewCell {
    
    @IBOutlet weak var flikrImg: UIImageView!
    @IBOutlet weak var flikrTitle: UILabel!
    
    var offline: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func configueCell(flickerPhotoModel: FlickrPhotoModel, index: Int) -> Void {
        
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        flikrTitle.text = flickerPhotoModel.title

        flikrImg.sd_setImage(with: URL(string: flickerPhotoModel.flickerImg) )
    }
    
    func configueOfflineCell(flickerPhotoModel: OfflineModel, index: Int) -> Void {
        
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        flikrTitle.text = flickerPhotoModel.title
        if let photoData = flickerPhotoModel.photo {
            flikrImg?.image = UIImage(data: photoData)?.fixOrientation()
            
        }
    }
    
    
    func setupView() -> Void {
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
    
}
