//
//  SavedCell.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/18/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import UIKit

class SavedCell: UICollectionViewCell {
    
    @IBOutlet weak var flikrImg: UIImageView!
    @IBOutlet weak var flikrTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func configueCell(flickerPhotoModel: OfflineModel, index: Int) -> Void {
        
        flikrImg.image = UIImage(named: "dark\(index)")
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        flikrTitle.text = flickerPhotoModel.title
        if let photoData = flickerPhotoModel.photo {
            flikrImg?.image = UIImage(data: photoData)?.fixOrientation()
            
        }
    }
    
    
    func setupView() -> Void {
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
    
}
