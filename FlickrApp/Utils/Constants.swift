//
//  Constants.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/17/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ success: Bool, _ error: String?) -> ()
let UNWIND = "unwindToMain"

let FLICKR_API_KEY = "92111faaf0ac50706da05a1df2e85d82"
let FLICKR_SECRET_KEY = "89ded1035d7ceb3a"


let FLICKR_URL = "https://api.flickr.com/services/rest/"
let PHOTOS_METHOD = "flickr.photos.getRecent"
//per page returns 100 on every page.
let PER_PAGE = 10
let PAGE = 1
let FORMAT_TYPE = "json"
let JSON_CALLBACK = 1


let NOTIFY_PHOTO_SELECTED = Notification.Name("notifyPhotoSelected")

let HEADER = [
    "Content-Type": "application/json; charset=utf-8"
]

//CELLS

let PHOTO_CELL = "photoCell"
let SAVED_CELL = "savedCell"
let DETAIL_VC = "detailVC"
let DETAIL_SAVED_VC = "detailSavedVC"
let SAVED_DETAIL_VC = "savedDetailVC"
let MAIN_TO_SAVED_DETAIL_VC_SEGUE = "mainSavedDetailVC"
let OFFLINE_TO_SAVED_DETAIL_VC_SEGUE = "offlineSavedDetailVC"
let DETAIL_VC_SEGUE = "detailVCSegue"

//Realm
let REALM_QUEUE = DispatchQueue(label: "realmQueue")
let REALM_RUN_CONFIG = "realmRunConfig.realm"

