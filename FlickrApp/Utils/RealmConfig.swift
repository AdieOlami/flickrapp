//
//  RealmConfig.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/18/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import Foundation
import RealmSwift

//THIS IS CONFIGURATION FOR REALM
class RealmConfig {
    
    static var realmDataConfig: Realm.Configuration {
        let realmPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(REALM_RUN_CONFIG)
        var config = Realm.Configuration(
            fileURL: realmPath,
            schemaVersion: 0,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 0) {
                    //Nothing to do
                    //Realm with automatically detect new properties and remove properties
                }
        })
        
        config.objectTypes = [OfflineModel.self]
        return config
    }
    
}
