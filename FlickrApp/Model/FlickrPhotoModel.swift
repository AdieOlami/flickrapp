//
//  FlickrPhotoModel.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/17/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import Foundation

//MARK: - Online Object
/***************************************************************/
struct FlickrPhotoModel {
    
    public private(set) var id: String!
    public private(set) var farm: String!
    public private(set) var server: String!
    public private(set) var secret: String!
    public private(set) var flickerImg: String!
    public private(set) var title: String!
}
