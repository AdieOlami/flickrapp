//
//  SavedModel.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/18/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import Foundation
import RealmSwift

//MARK: - Realm Object
/***************************************************************/
class OfflineModel: Object {
    
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var photo: Data? = nil
    @objc dynamic var title : String = ""
    @objc dynamic var createdDate: Date?
    
    
    override static func primaryKey() -> String {
        return "id"
    }
    
    
}
