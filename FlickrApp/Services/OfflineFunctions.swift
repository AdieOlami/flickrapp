//
//  OfflineStorage.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/18/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import UIKit
import RealmSwift

// I AM USING REALM BECAUSE IT IS MORE ROBUST COMPARED TO COREDATA AND USERDEFAULTS CANNOT STORE LARGE DETAILS
class OfflineFunctions {
    
    private var database:Realm
    static let instance = OfflineFunctions()

    var offlineItems: Results<OfflineModel>?
    var selectedImage : OfflineModel?
    
    private init() {
        database = try! Realm(configuration: RealmConfig.realmDataConfig)
    }
    
    //MARK:- Add Offline Function
    func addData(object: OfflineModel)   {
        
        try! database.write {
            database.add(object, update: true)
        }
    }
    
    //MARK:- Get Offline
    func getDataFromDB() -> Results<OfflineModel> {
        offlineItems = database.objects(OfflineModel.self)
        return offlineItems!
    }

    
    //MARK:- Create Offline
    func createOfflineList(createdDate: Date, photo: Data, title: String, completion: @escaping CompletionHandler) -> Void {
        
        REALM_QUEUE.sync {
            
            let offlineList = OfflineModel()
            
            offlineList.createdDate = createdDate
            offlineList.photo = photo
            offlineList.title = title
            OfflineFunctions.instance.addData(object: offlineList)
            completion(true, nil)
        }
        
    }
    
    
    func updateOfflineList(update: OfflineModel, createdDate: Date, photo: Data, title: String) -> Void {
        
        REALM_QUEUE.sync {
            try! self.database.write {
                update.createdDate = createdDate
                update.photo = photo
            }
        }
        
        
    }
 
    //MARK:- Delete OfflineList
    func deleteAllFromDatabase()  {
        try! database.write {
            database.deleteAll()
        }
    }
    
    
    func deleteFromDb(object: OfflineModel)   {
        try! database.write {
            database.delete(object)
        }
    }
}
