//
//  ServiceProvider.swift
//  FlickrApp
//
//  Created by Olar's Mac on 11/17/18.
//  Copyright © 2018 trybetech LTD. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ServiceProvider: NSObject {
    
    static let instance = ServiceProvider()
    
    var flickerPhotos = [FlickrPhotoModel]()
    var selectedPhoto: FlickrPhotoModel?
    

    //MARK: - Networking
    /***************************************************************/
    
    func getPhotos(completion: @escaping CompletionHandler) -> Void {
        
        let parameter: [String: Any] = [
            "method": PHOTOS_METHOD,
            "api_key": FLICKR_API_KEY,
            // Comment "per_page" to return 100
            "per_page": PER_PAGE,
            "page": PAGE,
            "format": FORMAT_TYPE,
            "nojsoncallback": JSON_CALLBACK]
        
        Alamofire.request(FLICKR_URL, method: .get, parameters: parameter, headers: HEADER).responseJSON {[weak self] response in

            if response.result.isSuccess {
                
                let result = response.result.value!
                let photoJson: JSON = JSON(result)
                
                self?.setData(json: photoJson)
                completion(true, nil)
                
            } else {
                let error = response.error?.localizedDescription

                completion(false, error)
            }
        }

    }
    
    //MARK: - JSON Parsing
    /***************************************************************/
    func setData(json: JSON) -> Void {
        
        if let photos = json["photos"]["photo"].array {
            for item in photos {
                
                let photoID = item["id"].stringValue
                let farm = item["farm"].stringValue
                let server = item["server"].stringValue
                let secret = item["secret"].stringValue
                let title = item["title"].stringValue
                
                let imageString = "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_b.jpg/"
                //could use _n.jpg too to get reduced quality
                
                let flickrPhoto = FlickrPhotoModel(id: photoID, farm: farm, server: server, secret: secret, flickerImg: imageString, title: title)
                flickerPhotos.append(flickrPhoto)
            }
        }

    }


}
